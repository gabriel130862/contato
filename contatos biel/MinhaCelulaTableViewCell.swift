//
//  MinhaCelulaTableViewCell.swift
//  contatos biel
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class MinhaCelulaTableViewCell: UITableViewCell {
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var email:UILabel!
    @IBOutlet weak var endereco:UILabel!
    @IBOutlet weak var numero:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
