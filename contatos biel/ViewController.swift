//
//  ViewController.swift
//  contatos biel
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit
struct Contato {
    let nome: String
    let numero: String
    let email: String
    let endereco: String
 
}
class ViewController: UIViewController,UITableViewDataSource {
    
    var listadecontatos: [Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listadecontatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MinhaCelulaTableViewCell
        let contato = listadecontatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        return cell 
    }
    

    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        
        listadecontatos.append(Contato(nome:"contato 1", numero: "31 666-666",email: "biel666devil@gmail.com", endereco: "013 campebell" ))
        listadecontatos.append(Contato(nome:"contato 2", numero: "31 666-666",email: "biel666devil@gmail.com", endereco: "013 campebell" ))
        listadecontatos.append(Contato(nome:"contato 3", numero: "31 666-666",email: "biel666devil@gmail.com", endereco: "013 campebell" ))
       
    }


}

